﻿using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
	/// <summary>
	/// Interaction logic for Korisnik_Putnik.xaml
	/// </summary>
	public partial class Korisnik_Putnik : Window
	{

		private Korisnik korisnik;
		public Korisnik_Putnik(Korisnik korisnik)
		{
			InitializeComponent();
			this.korisnik = korisnik;
		}

		private void BtnPegledKarata_Click(object sender, RoutedEventArgs e)
		{
			PregledKarata a = new PregledKarata(korisnik.KorisnickoIme);
			a.Show();
		}

		private void KupiKartu_Click(object sender, RoutedEventArgs e)
		{
			PregledLetovaa a = new PregledLetovaa("korisnik");


			a.Show();
		}

		private void Pregled_Click(object sender, RoutedEventArgs e)
		{
			Registracija a = new Registracija(null, "pregled", korisnik);
			a.Show();

		}

		private void Izmena_Click(object sender, RoutedEventArgs e)
		{
			Registracija a = new Registracija(null, "izmena", korisnik);
			a.Show();
		}
	}
}
