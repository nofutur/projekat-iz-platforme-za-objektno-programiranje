﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
	/// <summary>
	/// Interaction logic for Avio_kompanija.xaml
	/// </summary>
	public partial class Avio_kompanija : Window
	{
		public Avio_kompanija()
		{
			InitializeComponent();




		}



		private void Dodaj_Click(object sender, RoutedEventArgs e)
		{

			if (ProveraSifreAvioKompanije(Convert.ToInt16( sifraAvioKompanije.Text)))
			{
			}
			else return;

			if (ProveraImenaAvioKompanije(NazivAviokompanije.Text))
			{
			}
			else return;



			Aviokompanija a = new Aviokompanija();

			a.Sifra =Convert.ToInt16( sifraAvioKompanije.Text);
			a.NazivAviokompanije = NazivAviokompanije.Text;
			a.obrisano = false;

			Data.Instance.SacuvajAvioKomapniju(a);
			

			//foreach (var aa in Data.Instance.aviokompanija)

			//Console.WriteLine(aa.ToString());
			


			AvioKompanijaGlavna avio = new AvioKompanijaGlavna();
			avio.Show();
			this.Close();

		}



		private bool ProveraSifreAvioKompanije(int a)
		{

			//Data.Instance.UcitatisveLetove();
			foreach (var avioKompanije in Data.Instance.aviokompanija)
			{


				if (avioKompanije.Sifra == a)
				{

					MessageBox.Show("sifra avio-kompanije  vec postoji");
					return false;
				}
			}


			return true;
		}


		private bool ProveraImenaAvioKompanije(string a)
		{

			
			foreach (var avioKompanije in Data.Instance.aviokompanija)
			{


				if (avioKompanije.NazivAviokompanije == a)
				{

					MessageBox.Show("naziv  avio-kompanije  vec postoji");
					return false;
				}
			}


			return true;
		}



	}
}
