﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
	/// <summary>
	/// Interaction logic for PrikazIzmenaAvioKompanije.xaml
	/// </summary>
	public partial class PrikazIzmenaAvioKompanije : Window
	{

		public Aviokompanija aviokompanije;
		public PrikazIzmenaAvioKompanije(String pravac, Aviokompanija aviokompanije)
		{
			InitializeComponent();
			this.aviokompanije = aviokompanije;
			sifraAvioKompanije.IsEnabled = false;
			if (pravac == "Pregled")
			{
				NazivAvioKompanije.IsEnabled = false;
				izmenaa.IsEnabled = false;
			}


			NazivAvioKompanije.Text = aviokompanije.NazivAviokompanije;
			sifraAvioKompanije.Text = Convert.ToString( aviokompanije.Sifra);



		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{

			aviokompanije.NazivAviokompanije = NazivAvioKompanije.Text;
			aviokompanije.Sifra = Convert.ToInt16( sifraAvioKompanije.Text);

			izmena.Instance.izmenaAvioKompanije(aviokompanije);

			AvioKompanijaGlavna avio = new AvioKompanijaGlavna();
			avio.Show();
			this.Close();

		}

		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			AvioKompanijaGlavna avio = new AvioKompanijaGlavna();
			avio.Show();
			this.Close();
		}
	}
}
