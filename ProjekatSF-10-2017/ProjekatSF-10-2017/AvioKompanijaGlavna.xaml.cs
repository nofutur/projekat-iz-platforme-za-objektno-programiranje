﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
	/// <summary>
	/// Interaction logic for AvioKompanijaGlavna.xaml
	/// </summary>
	public partial class AvioKompanijaGlavna : Window
	{


		List<Aviokompanija> aviokompanija = new List<Aviokompanija>();
		public AvioKompanijaGlavna()
		{
			InitializeComponent();

			PrikazAvioKompanije.Items.Refresh();
			Reload();

			PrikazAvioKompanije.ItemsSource = aviokompanija;
			PrikazAvioKompanije.IsSynchronizedWithCurrentItem = true;
			PrikazAvioKompanije.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

			
		}
		public void Reload()
		{

			aviokompanija.Clear();
			Data.Instance.UcitajAvioKompaniju();
			foreach (Aviokompanija avionKompniju in Data.Instance.aviokompanija)
				if (avionKompniju.obrisano == false)
					aviokompanija.Add(avionKompniju);


		}




		private void DodajAvioKompaniju_Click(object sender, RoutedEventArgs e)
		{
			Avio_kompanija a = new Avio_kompanija();
			a.Show();
			this.Close();

		}

		private void Izmena_Click(object sender, RoutedEventArgs e)
		{

			Aviokompanija avio = (Aviokompanija)PrikazAvioKompanije.SelectedValue;
		
			PrikazIzmenaAvioKompanije a = new PrikazIzmenaAvioKompanije("izmena",avio);
			a.Show();
			this.Close();


		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Aviokompanija avio = (Aviokompanija)PrikazAvioKompanije.SelectedValue;

			PrikazIzmenaAvioKompanije a = new PrikazIzmenaAvioKompanije("Pregled", avio);
			a.Show();
			this.Close();
		}


		private void Obrisi_Click(object sender, RoutedEventArgs e)
		{

			if (PrikazAvioKompanije.SelectedValue == null)
			{
				MessageBox.Show("Odaberi avio-kompaniju");
			}

			else if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
			{



				Aviokompanija aviokompanije = (Aviokompanija)PrikazAvioKompanije.SelectedItem;

				
				aviokompanija.Remove(aviokompanije);
				
				
				aviokompanije.obrisano = true;
				izmena.Instance.izmenaAvioKompanije(aviokompanije);
				PrikazAvioKompanije.Items.Refresh();
			}


		}
	}
}
