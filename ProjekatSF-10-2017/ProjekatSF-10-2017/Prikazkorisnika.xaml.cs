﻿using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
    /// <summary>
    /// Interaction logic for Prikazkorisnika.xaml
    /// </summary>
    public partial class Prikazkorisnika : Window
    {

		Korisnik kor = new Korisnik();
        public Prikazkorisnika(Korisnik kor)
        {

			kor = kor;

			InitializeComponent();


			KorisnickoIme.IsEnabled = false;
			gmail.IsEnabled = false;
			ime.IsEnabled = false;
			Prezime.IsEnabled = false;
			Adresa.IsEnabled = false;
			Pol.IsEnabled = false;


			KorisnickoIme.Text = kor.KorisnickoIme;
			gmail.Text = kor.Gmail;
			ime.Text = kor.Ime;
			Prezime.Text = kor.Prezime;
			Adresa.Text = kor.Adresa;
			Pol.Text = kor.Pol;

		}
    }
}
