﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ProjekatSF_10_2017.modeli;
using ProjekatSF_10_2017.database;
namespace ProjekatSF_10_2017.LetPrikazi1
{
    /// <summary>
    /// Interaction logic for PregledPojedinacnihLetova1.xaml
    /// </summary>
    public partial class PregledPojedinacnihLetova1 : Window
    {
		public Let let;
		public String odabir;



		public PregledPojedinacnihLetova1(Let let, String odabir)
        {
            InitializeComponent();
			this.let = let;
			this.odabir = odabir;

			BrojLeta.IsEnabled = false;

				BrojLeta.Text = let.BrojLeta;
				VremePolaska.Text = let.VremePolaska;
				VremeDolaska.Text = let.VremeDolaska;
				Odrediste.Text = let.Odrediste;
				Destinacija.Text = let.Destinacija;
				Cena.Text = Convert.ToString( let.CenaKarte);


			if (odabir == "pregled")
			{
				Izmeni.IsEnabled = false;

				
				VremePolaska.IsEnabled = false;
				VremeDolaska.IsEnabled = false;
				Odrediste.IsEnabled = false;
				Cena.IsEnabled = false;
				Destinacija.IsEnabled = false;

			}
			
	

		}

		private void Odustani_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		private void Izmeni_Click(object sender, RoutedEventArgs e)
		{

			Let l = new Let();

			l.BrojLeta= BrojLeta.Text;
			l.VremePolaska = VremePolaska.Text;
			l.VremeDolaska =VremeDolaska.Text;
			l.Odrediste = Odrediste.Text;
			l.Destinacija = Destinacija.Text;
			l.CenaKarte =Convert.ToDecimal( Cena.Text);
			l.Obrisan = false;


			izmena.Instance.IzmeniLetove(l);

			this.Close();




		}
	}
}
