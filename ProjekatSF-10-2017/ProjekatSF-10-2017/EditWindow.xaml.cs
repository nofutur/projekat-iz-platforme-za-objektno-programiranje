﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
    /// <summary>
    /// Interaction logic for EditWindow.xaml
    /// </summary>
    public partial class EditWindow : Window
    {
        
            public enum Opcija { DODAVANJE, IZMENA }
            Aerodrom aerodrom;
            Opcija opcija;

        public object LBAerodromi { get; private set; }




		public EditWindow(Aerodrom aerodrom, Opcija opcija)

		{


                InitializeComponent();
                this.aerodrom = aerodrom;
                this.opcija = opcija;


			if (opcija.Equals(Opcija.DODAVANJE))
			{
				BtnIzmeni.IsEnabled = false;
			}


			if (opcija.Equals(Opcija.IZMENA))
            {
				btnSacuvaj.IsEnabled = false;

				txtSifra.IsEnabled = false;

				txtSifra.Text = Convert.ToString(aerodrom.Sifra);
				txtGrad.Text = aerodrom.Grad;
				txtNaziv.Text = aerodrom.Naziv;

			}


        }

            private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
            {


			Aerodrom aa = new Aerodrom();


				aa.Sifra = Convert.ToInt16(txtSifra.Text);
				aa.Naziv = txtNaziv.Text;
				aa.Grad = txtNaziv.Text;
				aa.Obrisano = false;



				this.Close();
				Data.Instance.SacuvajSveAerodrome(aa);

				AerodromMeni g = new AerodromMeni();
				g.Show();	
				




            }

		private void btnOdustani_Click(object sender, RoutedEventArgs e)
		{

			this.Close();
			AerodromMeni g = new AerodromMeni();

			g.Show();
			


		}

		private void Izmeni_Click(object sender, RoutedEventArgs e)
		{
			Aerodrom aa = new Aerodrom();
			aa.Sifra = Convert.ToInt16(txtSifra.Text);
			aa.Naziv = txtNaziv.Text;
			aa.Grad = txtNaziv.Text;
			aa.Obrisano = false;
			izmena.Instance.izmenaAerodrom(aa);

			this.Close();
			AerodromMeni g = new AerodromMeni();
			g.Show();
		


		}
	}
}

