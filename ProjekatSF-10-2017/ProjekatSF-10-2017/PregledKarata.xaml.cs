﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
	
	/// <summary>
	/// Interaction logic for PregledKarata.xaml
	/// </summary>
	public partial class PregledKarata : Window
	{
		ICollectionView prikaz;
		private string a;
		public PregledKarata(string a)
		{
		
			InitializeComponent();
			this.a = a;

			Data.Instance.ucitajKarte();
			List<karta> kartaList = new List<karta>();

			foreach (var kartaa in Data.Instance.karte)
			{
				
				if (kartaa.nazivPutnika.Equals(a))
				{
					kartaList.Add(kartaa);
				}
			}


			prikaz = CollectionViewSource.GetDefaultView(kartaList);

			pregledKarata.ItemsSource = prikaz;
			pregledKarata.IsSynchronizedWithCurrentItem = true;
			pregledKarata.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);



		}

		private void PregledKarata_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{


			Data.Instance.ucitajKarte();
			List<karta> kartaList = new List<karta>();

			foreach (var kartaa in Data.Instance.karte)
			{
				Console.WriteLine("+++++++---*****"+ kartaa.nazivPutnika);
				if (kartaa.nazivPutnika.Equals(a))
				{
					kartaList.Add(kartaa);
				}
			}


			prikaz = CollectionViewSource.GetDefaultView(kartaList);

			pregledKarata.ItemsSource = prikaz;
			pregledKarata.IsSynchronizedWithCurrentItem = true;
			pregledKarata.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);


			
		}
	}
}
