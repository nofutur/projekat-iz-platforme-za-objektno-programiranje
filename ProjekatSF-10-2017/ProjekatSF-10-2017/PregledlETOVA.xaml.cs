﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
    /// <summary>
    /// Interaction logic for PregledlETOVA.xaml
    /// </summary>
    public partial class PregledlETOVA : Window
    {

        ICollectionView view;

        public PregledlETOVA()
        {
           
            InitializeComponent();

            Data.Instance.UcitatisveLetove();
            view = CollectionViewSource.GetDefaultView(Data.Instance.letovi);
            


            DGAerodromi.ItemsSource = view; //dodati ispis,uslov za aerodrome koji nisu obrisani
            DGAerodromi.IsSynchronizedWithCurrentItem = true; //kada kliknemo na jedan aerodrom da se taj selektuje
            DGAerodromi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);



            BoxbrojLeta.Text = "Po broju leta";
            BoxAvioKompanija.Text = "Po aviokompani";
            MinCena.Text = "min cena";
            maxCena.Text = "max cena";
            BoxOdrediste.Text = "Odrediste";
            BoxDestinaci.Text = "Destinacija";

            







        }
        

        public void BrojLetaPretraga()
        {
            foreach (var let in Data.Instance.letovi)
            {
                if (let.Equals(BoxbrojLeta.Text))
                {

                    view = CollectionViewSource.GetDefaultView(let);
                    DGAerodromi.ItemsSource = view;             
                    DGAerodromi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
                }
            }


        }
        public void AviokompanijaPretraga()
        {
            foreach (var let in Data.Instance.letovi)
            {
                if (let.BrojLeta.Equals(BoxAvioKompanija.Text))
                {

                    view = CollectionViewSource.GetDefaultView(let);
                    DGAerodromi.ItemsSource = view;
                    DGAerodromi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
                }
            }


        }
        public void OdredistePretraga()
        {
            foreach (var let in Data.Instance.letovi)
            {
                if (let.Odrediste.Equals(BoxOdrediste.Text))
                {

                    view = CollectionViewSource.GetDefaultView(let);
                    DGAerodromi.ItemsSource = view;
                    DGAerodromi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
                }
            }


        }


        private void MaxCena_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            maxCena.Text = "";
        }

        private void MinCena_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MinCena.Text = "";
        }

        private void BoxAvioKompanija_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BoxAvioKompanija.Text = "";
        }

        private void BoxbrojLeta_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BoxbrojLeta.Text = "";
        }

        private void BoxOdrediste_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BoxOdrediste.Text = "";
        }

        private void BoxDestinaci_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BoxDestinaci.Text = "";
        }

        private void BoxbrojLeta_MouseLeave(object sender, MouseEventArgs e)
        {
            if (BoxbrojLeta.Text == "")
            {
                BoxbrojLeta.Text = "Po broju leta";
            }
            
               
            
}

        private void BoxAvioKompanija_MouseLeave(object sender, MouseEventArgs e)
        {
            if (BoxAvioKompanija.Text == "")
            {
                BoxAvioKompanija.Text = "Po broju leta";
            }
        }

        private void BoxbrojLeta_TextChanged(object sender, TextChangedEventArgs e)
        {
            BrojLetaPretraga();
        }

        private void BoxAvioKompanija_TextChanged(object sender, TextChangedEventArgs e)
        {
            AviokompanijaPretraga();
        }

        private void BoxOdrediste_TextChanged(object sender, TextChangedEventArgs e)
        {
            OdredistePretraga();
        }
    }
}
