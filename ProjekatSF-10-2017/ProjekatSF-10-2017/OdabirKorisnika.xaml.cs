﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
    /// <summary>
    /// Interaction logic for OdabirKorisnika.xaml
    /// </summary>
    public partial class OdabirKorisnika : Window
    {
		private Let let;
		List<Korisnik> KorisnikList = new List<Korisnik>();
		public OdabirKorisnika(Let let)
        {

			let = let;
            InitializeComponent();
			Reload();
			PrikazKorisnika.ItemsSource = KorisnikList;


			PrikazKorisnika.IsSynchronizedWithCurrentItem = true;
			PrikazKorisnika.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);


		}


		public void Reload()
		{


			Data.Instance.UcitajSveKorisnike();
			KorisnikList.Clear();
			foreach (Korisnik korisnik in Data.Instance.Korisnici)
				if (korisnik.obrisan == false)
					KorisnikList.Add(korisnik);


		}


		private void Dalje_Click(object sender, RoutedEventArgs e)
		{
			Korisnik korisnik = (Korisnik)PrikazKorisnika.SelectedItem;
			odabirsedista a = new odabirsedista(let, korisnik);
			a.Show();
			this.Close();


		}

		private void Nazad_Click(object sender, RoutedEventArgs e)
		{

			PregledLetovaa a = new PregledLetovaa("administrator");
			a.Show();
			this.Close();


		}
	}
}
