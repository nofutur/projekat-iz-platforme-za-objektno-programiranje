﻿using ProjekatSF_10_2017.avion;
using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
    /// <summary>
    /// Interaction logic for Avion.xaml
    /// </summary>
    public partial class Avionn : Window
    {
		//private string putanja;

		public Avionn()
        {
            InitializeComponent();



			//Data.Instance.UcitajAvioKompaniju();

			UnosSifreLetova();

			UnosSifreAviona();

		}
				
		

		private void UnosSifreLetova ()
		{
			Data.Instance.UcitatisveLetove();
			foreach (var let in Data.Instance.letovi)
			{

					sifraLeta.Items.Add(let.BrojLeta);
				
			}
			
		}
		private void UnosSifreAviona()
		{
			Data.Instance.UcitajAvioKompaniju();
			foreach (var aviokompanija in Data.Instance.aviokompanija)
			{

				AvioKompanija.Items.Add(aviokompanija.NazivAviokompanije);

			}

		}



		private void Snimi_Click(object sender, RoutedEventArgs e)
		{

			if(SifraAvionaa.Text.Length != 3){
				MessageBox.Show("Sifra aviona mora da sadrzi tacno 3 cifre");
				return;
			}


			if (ProveraSifreAviona(SifraAvionaa.Text))
			{


			}
			else return;
			Data.Instance.UcitajAvione();
			foreach (var avion in Data.Instance.avioni)
				Console.WriteLine(avion.ToString());



			Avion a = new Avion();
			a.PilotIme = ImePilota.Text;
			a.PilotPrezime = PrezimePilota.Text;
			
			a.sifraAviona = SifraAvionaa.Text;
			a.SifraBiznisKlase = StvaranjBrojaSedistaBiznisKlase();
			a.brojKolonaBiznis = BizniskaKolone.Text;
			a.brojRedaBiznis = BiznisRedovi.Text;
			a.brojKolonaEkonomska = EkonomskaKolone.Text;
			a.brojRedaEkonomska = EkonomskaRedovi.Text;
			a.SifraEkonomskeKlase = StvaranjBrojaSedistaEkonomskeKlase();
			a.NazivAvioKompanije = AvioKompanija.Text;
			a.obrisan = false;

			
			




			avionGlavni aa = new avionGlavni();
			aa.Show();




		}


		private bool ProveraSifreAviona(string a)
		{

			//Data.Instance.UcitatisveLetove();
			foreach (var avion in Data.Instance.avioni)
			{

				
				if (avion.sifraAviona == a)
				{
					
					MessageBox.Show("sifra aviona ve postoji");
					return false;
				}
			}

		
			return true;
		}






		private bool ProverabrojLeta(string a)
		{

			Data.Instance.UcitatisveLetove();
			foreach (var let in Data.Instance.letovi)
			{

				Console.WriteLine(let.BrojLeta);
				Console.WriteLine(a);
				if (let.BrojLeta == a)
				{
					return true;
				}
			}
			
			MessageBox.Show("sifra leta ne postoji");
			return false;
		}

		


		private string StvaranjBrojaSedistaBiznisKlase() {
			Random rnd = new Random();

		int a=0;
		Boolean c = true;
			while (c)
			{
				a = rnd.Next(99, 1000);
			

				foreach (var avion in Data.Instance.avioni)
				{
					if (avion.SifraBiznisKlase == Convert.ToString(a))
					{
						c = false;


					}


				}
				
				c = false;

			}
		return Convert.ToString(a);
		}


		private string StvaranjBrojaSedistaEkonomskeKlase()
		{
		Random rnd = new Random();

		int a=0;
		
			Boolean c = true;
			while (c)
			{
				a = rnd.Next(99, 1000);

				foreach (var avion in Data.Instance.avioni)
				{
					if (avion.SifraEkonomskeKlase == Convert.ToString(a))
					{

						break;

					}
				}

			c = false;

			} 
		return Convert.ToString(a);
			
		}

		private void SifraAviona_TextChanged(object sender, TextChangedEventArgs e)
		{

		}

		private void SifraLeta_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{

		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			avionGlavni aa = new avionGlavni();
			aa.Show();
			this.Close();
		}



		







		private bool ProveraSifreAvionaa(string a)
		{
			Data.Instance.UcitajAvione();

			foreach (var avion in Data.Instance.avioni)
			{


				if (avion.sifraAviona == a)
				{

					return true;
				}



			}

			MessageBox.Show("sifra aviona ne postoji");
			return false;
		}



	}
}
