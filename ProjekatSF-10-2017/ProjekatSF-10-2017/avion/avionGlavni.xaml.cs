﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017.avion
{
	/// <summary>
	/// Interaction logic for avionGlavni.xaml
	/// </summary>
	public partial class avionGlavni : Window
	{
		
		public bool IsEnabled { get; set; }


		//public List<Avion> avionlist { get; set; }
		List<Avion> avionlist = new List<Avion>();

		public avionGlavni()
		{


			InitializeComponent();

			PrikazAviona.Items.Refresh();
			Reload();
			
			PrikazAviona.ItemsSource = avionlist;
			PrikazAviona.IsSynchronizedWithCurrentItem = true;
			PrikazAviona.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);



		}

		
		public void Reload ()
		{


			Data.Instance.UcitajAvione();
			
			foreach (Avion avion in Data.Instance.avioni)
				if (avion.obrisan == false)
					avionlist.Add(avion);


		}

	

		
		private int IndeksSelektovogAerodroma(String sifra)
		{
			var indeks = -1;
			for (int i = 0; i < Data.Instance.avioni.Count; i++)
			{
				if (Data.Instance.avioni[i].sifraAviona.Equals(sifra))
				{
					indeks = i;
					break;
				}
			}
			return indeks;

		}

		private bool SelektovanAerodrom(Avion avion)
		{
			if (avion == null)
			{
				MessageBox.Show("Nije selektovan aerodrom");
				return false;
				
			}

			return true;
		}


		private void BtnDodaj_Click_1(object sender, RoutedEventArgs e)
		{
			this.Close();
			Avionn a = new Avionn();
			a.Show();
		}

		private void btnPojedinacniPrikaz_Click(object sender, RoutedEventArgs e)
		{
			if (SelektovanAerodrom((Avion)PrikazAviona.SelectedItem)==true) {
			AvionPrikaz a = new AvionPrikaz((Avion)PrikazAviona.SelectedItem, "pregled");
			a.Show(); 
			}

			this.Close();
		}

		private void LBAerodromi_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{

		}

		private void BtnIzmeni_Click_1(object sender, RoutedEventArgs e)
		{
			if (SelektovanAerodrom((Avion)PrikazAviona.SelectedItem) == true)
			{
				AvionPrikaz a = new AvionPrikaz((Avion)PrikazAviona.SelectedItem, "izmena");
				a.Show();
			}

			
			this.Close();


		}


		private void BtnObrisi_Click_1(object sender, RoutedEventArgs e)
		{

			if (PrikazAviona.SelectedValue == null)
			{
				MessageBox.Show("Odaberi let");
			}

			else if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
				{



				Avion avion = (Avion)PrikazAviona.SelectedItem;
					brisanje.Instance.obrisiAvion(avion);

				Avion aaa = (Avion)PrikazAviona.SelectedValue;
				avionlist.Remove(aaa);
				PrikazAviona.Items.Refresh();

			}
						

		}

		private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{

		}

	

		private void AvioKompanija_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (AvioKompanija.Text == "")
			{
				Reload();
			}
			else
			{
				Data.Instance.UcitajSveKorisnike();
				avionlist.Clear();
				foreach (Avion avion in Data.Instance.avioni)
					if (avion.NazivAvioKompanije.Contains(AvioKompanija.Text))
						avionlist.Add(avion);
				PrikazAviona.Items.Refresh();
			}
		}
	}
}
