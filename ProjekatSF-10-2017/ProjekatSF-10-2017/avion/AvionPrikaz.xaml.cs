﻿using ProjekatSF_10_2017.avion;
using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
    /// <summary>
    /// Interaction logic for AvionPrikaz.xaml
    /// </summary>
    public partial class AvionPrikaz : Window
    {
		public Avion avion;
		public string Opcija;


		public AvionPrikaz(Avion avion,string opcija)
        {
			Opcija = opcija;


			InitializeComponent();
			this.avion = avion;
			
			PilotIme.Text = avion.PilotIme;
			PilotPrezime.Text = avion.PilotPrezime;
			SifraAviona.Text = avion.sifraAviona;
			AvioKompanija.Text = avion.NazivAvioKompanije;
			SifraEkonomske.Text = avion.SifraEkonomskeKlase;
			SifraBiznis.Text = avion.SifraBiznisKlase;
			/*
			if (Opcija == "izmena")
			{


				SifraLeta.Text = avion.BrojLeta;
				PilotIme.Text = avion.PilotIme;
				PilotPrezime.Text = avion.PilotPrezime;
				SifraAviona.Text = avion.sifraAviona;
				AvioKompanija.Text = avion.NazivAvioKompanije;
				SifraEkonomske.Text = avion.SifraEkonomskeKlase;
				SifraBiznis.Text = avion.SifraBiznisKlase;

			}*/





			if (Opcija == "pregled")
			{

				SifraLeta.IsEnabled = false;
				PilotIme.IsEnabled = false;
				PilotPrezime.IsEnabled = false;
				SifraAviona.IsEnabled = false;
				AvioKompanija.IsEnabled = false;
				SifraEkonomske.IsEnabled = false;
				SifraBiznis.IsEnabled = false;


				Izmeni.IsEnabled = false;




			}



		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{

			Avion avion = new Avion();


			
			avion.PilotIme = PilotIme.Text;
			avion.PilotPrezime= PilotPrezime.Text;
			 avion.sifraAviona= SifraAviona.Text;
			avion.NazivAvioKompanije= AvioKompanija.Text;
			avion.SifraEkonomskeKlase= SifraEkonomske.Text;
			avion.SifraBiznisKlase= SifraBiznis.Text;



			izmena.Instance.izmenaAvion(avion);
			avionGlavni aa = new avionGlavni();
			aa.Show();
			this.Close();
		}

		private void Button_Click_1(object sender, RoutedEventArgs e)
		{

			avionGlavni aa = new avionGlavni();
			aa.Show();
			this.Close();
		}
	}
}
