﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
    /// <summary>
    /// Interaction logic for KartaGlavna.xaml
    /// </summary>
    public partial class KartaGlavna : Window
    {

		List<karta> KartaList = new List<karta>();



		public KartaGlavna()
        {

			InitializeComponent();
			PrikazKarte.Items.Refresh();
			

			PrikazKarte.ItemsSource = KartaList;
			PrikazKarte.IsSynchronizedWithCurrentItem = true;
			PrikazKarte.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
			



			Reload();
			
        }


		public void Reload()
		{

			KartaList.Clear();
			Data.Instance.ucitajKarte();

			foreach (karta karta in Data.Instance.karte)
				if (karta.obrisan == false)
					KartaList.Add(karta);

			PrikazKarte.Items.Refresh();
		}




		private void Button_Click(object sender, RoutedEventArgs e)
		{
			PregledLetovaa a = new PregledLetovaa("administrator");
			a.Show();
			this.Close();
		}





		private void Button_Click_1(object sender, RoutedEventArgs e)
		{

			if (PrikazKarte.SelectedValue == null)
			{
				MessageBox.Show("Odaberi kartu");
			}

			else if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
			{

				karta karta = (karta)PrikazKarte.SelectedValue;

				Sediste sediste = new Sediste();

				sediste.BrojKolone = Convert.ToInt16( karta.brojkolone);
				sediste.BrojReda = Convert.ToInt16(karta.brojReda);
				sediste.sifraLeta = karta.brojLeta;
				sediste.zauzetost = false;
				sediste.korisnickoIme = "nije-uneto";

				izmena.Instance.IzmenaSedista(sediste);

				karta.obrisan = true;

				izmena.Instance.IzmenaKarte(karta);

				KartaList.Remove(karta);
				PrikazKarte.Items.Refresh();
				

			}




		}

		private void PregledKarte_Click(object sender, RoutedEventArgs e)
		{
			karta karta = (karta)PrikazKarte.SelectedValue;

			if (SelektovanAerodrom(karta)){

				PregledKarte a = new PregledKarte(karta);
				a.Show();
				this.Close();
			}


		}


		private bool SelektovanAerodrom(karta karta)
		{
			if (karta == null)
			{
				MessageBox.Show("Nije selektovan Karta");
				return false;

			}

			return true;
		}

		private void BrojLeta_TextChanged(object sender, TextChangedEventArgs e)
		{
			
		
			if (brojLeta.Text =="")
			{
				Reload();
			}
			else
			{

				KartaList.Clear();
				foreach (karta karte in Data.Instance.karte)
					if (karte.brojLeta.Contains(brojLeta.Text))
						if (karte.obrisan == false)
							KartaList.Add(karte);

				
			}
		PrikazKarte.Items.Refresh();
			
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{

			if (nazivPutnik.Text == "")
			{
				Reload();
			}
			else
			{

				KartaList.Clear();
				foreach (karta karte in Data.Instance.karte)
					if (karte.nazivPutnika.Contains(nazivPutnik.Text))
						if (karte.obrisan == false)
							KartaList.Add(karte);

				PrikazKarte.Items.Refresh();
			}
		}

		
	}
}
