﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ProjekatSF_10_2017;
using ProjekatSF_10_2017.avion;
namespace ProjekatSF_10_2017
{
	/// <summary>
	/// Interaction logic for administrator_korisnik.xaml
	/// </summary>
	public partial class administrator_korisnik : Window
	{
		public bool IsEnabled { get; set; }


		public administrator_korisnik()
		{
			InitializeComponent();
		}


		private void Avion_Click(object sender, RoutedEventArgs e)
		{
			avionGlavni a = new avionGlavni();
			a.Show();
			this.IsEnabled = false;
		}


		private void Aerodrom_Click(object sender, RoutedEventArgs e)
		{
			AerodromMeni aa = new AerodromMeni();
			aa.Show();
		}

		private void Let_Click(object sender, RoutedEventArgs e)
		{
			LetPrikaz l = new LetPrikaz();
			l.Show();

		}

		private void Korisnik_Click(object sender, RoutedEventArgs e)
		{
			KorisnikGlavno a = new KorisnikGlavno();
			a.Show();
		}

		private void Sedista_Click(object sender, RoutedEventArgs e)
		{

		}

		private void Karta_Click(object sender, RoutedEventArgs e)
		{
			KartaGlavna a = new KartaGlavna();
			a.Show();
		}

		private void Sviokompanija_Click(object sender, RoutedEventArgs e)
		{
			AvioKompanijaGlavna avio = new AvioKompanijaGlavna();
			avio.Show();


		}
	}
}
