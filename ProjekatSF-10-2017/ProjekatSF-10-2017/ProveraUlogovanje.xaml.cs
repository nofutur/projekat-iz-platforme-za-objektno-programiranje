﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
	/// <summary>
	/// Interaction logic for ProveraUlogovanje.xaml
	/// </summary>
	public partial class ProveraUlogovanje : Window
	{

		Let let;

		public ProveraUlogovanje(Let lett)
		{
			InitializeComponent();

			let = lett;
		}







		private void Registracija_Click(object sender, RoutedEventArgs e)
		{
			Registracija reg = new Registracija(let,"letovi",null);
			reg.Show();
			this.Close();
		}



		private void Dalje_Click(object sender, RoutedEventArgs e)
		{


			Data.Instance.UcitajSveKorisnike();

			foreach (var korisnik in Data.Instance.Korisnici)

			{
				
				if (korisnik.Lozinka == lozinka.Password && korisnik.KorisnickoIme == KorisnickoIme.Text)
				{
					
					odabirsedista a = new odabirsedista(let, korisnik);
				
					a.Show();
					this.Close();
					break;


				}
			}


		}
	}
}
