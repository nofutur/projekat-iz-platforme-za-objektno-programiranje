﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
	/// <summary>
	/// Interaction logic for AerodromMeni.xaml
	/// </summary>
	public partial class AerodromMeni : Window
	{

		List<Aerodrom> aerodromList = new List<Aerodrom>();


		public AerodromMeni()
		{
			InitializeComponent();

			Data.Instance.UcitajAerodrome();
			
			Reload();

			AerodromPrikaz.ItemsSource = aerodromList;
			AerodromPrikaz.IsSynchronizedWithCurrentItem = true;
			AerodromPrikaz.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

		}



		public void Reload()
		{
			Data.Instance.UcitajAerodrome();


			AerodromPrikaz.Items.Refresh();
			foreach (var aerodrom in Data.Instance.Aerodromi)
			{
				if (!aerodrom.Obrisano)
				{

					aerodromList.Add(aerodrom);

				}
			}
		}




		private void BtnDodaj_Click_1(object sender, RoutedEventArgs e)
		{
			EditWindow ew = new EditWindow(new Aerodrom(), EditWindow.Opcija.DODAVANJE);

			ew.Show();
			Reload();
			this.Close();
			
			
		}




		private void BtnIzmeni_Click_1(object sender, RoutedEventArgs e)
		{
			Aerodrom aerodrom = (Aerodrom)AerodromPrikaz.SelectedItem;
			if (SelektovanAerodrom(aerodrom))
			{
				EditWindow ew = new EditWindow(aerodrom, EditWindow.Opcija.IZMENA);
				ew.Show();
				Reload();
				this.Close();
				
			}
		}




		private void BtnObrisi_Click_1(object sender, RoutedEventArgs e)
		{
			if (AerodromPrikaz.SelectedValue == null)
			{
				MessageBox.Show("Odaberi let");
			}

			else if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
			{



				Aerodrom aerodrom = (Aerodrom)AerodromPrikaz.SelectedItem;
				aerodromList.Remove(aerodrom);

				aerodrom.Obrisano = true;
				izmena.Instance.izmenaAerodrom(aerodrom);

				
				AerodromPrikaz.Items.Refresh();



			}
		}





		private bool SelektovanAerodrom(Aerodrom aerodrom)
		{
			if (aerodrom == null)
			{
				MessageBox.Show("Nije selektovan aerodrom");
				return false;
			}

			return true;
		}

		private void btnPojedinacniPrikaz_Click(object sender, RoutedEventArgs e)
		{
			Aerodrom aerodrom = (Aerodrom)AerodromPrikaz.SelectedItem; //pribavi selektovani aerodrom
			if (SelektovanAerodrom(aerodrom))
			{

				PrikazAerodroma cc = new PrikazAerodroma(aerodrom);
				cc.Show();

			}
		}

		private void LBAerodromi_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{

		}
	}
}


