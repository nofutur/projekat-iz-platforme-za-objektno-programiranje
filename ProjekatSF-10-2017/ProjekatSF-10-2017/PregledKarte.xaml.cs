﻿using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
	/// <summary>
	/// Interaction logic for PregledKarte.xaml
	/// </summary>
	public partial class PregledKarte : Window
	{


		//karta karta = new karta();
		public PregledKarte(karta karta)
		{
		
			InitializeComponent();

			KorisnickoIme.IsEnabled = false;
			SifraLeta.IsEnabled = false;
			BrojKolone.IsEnabled = false; 
			BrojReda.IsEnabled = false;
			cena.IsEnabled = false;
			Kapija.IsEnabled = false;
			KlasaSedista.IsEnabled = false;

			KorisnickoIme.Text = karta.nazivPutnika;
			SifraLeta.Text = karta.brojLeta;
			BrojKolone.Text = karta.brojkolone;
			BrojReda.Text = karta.brojReda;
			cena.Text = Convert.ToString( karta.cenakarte);
			Kapija.Text = karta.kapija;
			KlasaSedista.Text = karta.klasaSedišta;




		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{

			KartaGlavna K = new KartaGlavna();
			K.Show();
			this.Close();

		}


		


	}
}
