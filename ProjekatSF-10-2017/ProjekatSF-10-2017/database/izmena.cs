﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatSF_10_2017.database
{


	 class izmena
	{
		

		 public izmena()
		{

		}

		private static izmena _instance = null;
		public static izmena Instance
		{
			get
			{
				if (_instance == null)

					_instance = new izmena();
				return _instance;

			}
		}

		public void IzmenaSedista(Sediste sediste)
		{
			using (SqlConnection conn = new SqlConnection())
			{
				conn.ConnectionString = Data.Instance.putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();

				sediste.zauzetost = true;
				command.CommandText = @"update sediste set zauzetost=@zauzetost , korisnickoime=@korisnickoIme where BrojKolone=@BrojKolone and BrojReda= @BrojReda and sifraLeta =@sifraLeta";


				Console.WriteLine("stupdate sediste set zauzetost=@zauzetost , ko++++++++++"+ sediste);



				command.Parameters.Add(new SqlParameter("@sifraAviona", sediste.SifraAviona));
				command.Parameters.Add(new SqlParameter("@BrojKolone", sediste.BrojKolone));
				command.Parameters.Add(new SqlParameter("@BrojReda", sediste.BrojReda));
				command.Parameters.Add(new SqlParameter("@zauzetost", sediste.zauzetost));
				command.Parameters.Add(new SqlParameter("@korisnickoIme", sediste.korisnickoIme));
				command.Parameters.Add(new SqlParameter("@sifraLeta", sediste.sifraLeta));
				command.ExecuteNonQuery();
			}


		}

		public void izmenaAvion(Avion avion)
		{
			using (SqlConnection conn = new SqlConnection())
			{
				conn.ConnectionString = Data.Instance.putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();


				command.CommandText = @"update avion set BrojLeta=@BrojLeta ,NazivAvioKompanije=@NazivAvioKompanije,PilotIme=@PilotIme, PilotPrezime=@PilotPrezime,sifraAviona=@sifraAviona ,SifraEkonomskeKlase=@SifraEkonomskeKlase,SifraBiznisKlase=@SifraBiznisKlase where sifraAviona=@sifraAviona";

				
				command.Parameters.Add(new SqlParameter("@NazivAvioKompanije", avion.NazivAvioKompanije));
				command.Parameters.Add(new SqlParameter("@obrisan", true));
				command.Parameters.Add(new SqlParameter("@PilotIme", avion.PilotIme));
				command.Parameters.Add(new SqlParameter("@PilotPrezime", avion.PilotPrezime));
				command.Parameters.Add(new SqlParameter("@sifraAviona", avion.sifraAviona));
				command.Parameters.Add(new SqlParameter("@SifraEkonomskeKlase", avion.SifraEkonomskeKlase));
				command.Parameters.Add(new SqlParameter("@SifraBiznisKlase", avion.SifraBiznisKlase));
				command.ExecuteNonQuery();


			}


		}
		public void izmenaAerodrom(Aerodrom aerodrom)
		{
			using (SqlConnection conn = new SqlConnection())
			{
				conn.ConnectionString = Data.Instance.putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();


				command.CommandText = @"update aerodrom set Grad=@Grad ,Naziv=@Naziv,Obrisano=@Obrisano where sifra=@sifra";

				command.Parameters.Add(new SqlParameter("@Grad", aerodrom.Grad));
				command.Parameters.Add(new SqlParameter("@Naziv", aerodrom.Naziv));
				command.Parameters.Add(new SqlParameter("@Obrisano", aerodrom.Obrisano));
				command.Parameters.Add(new SqlParameter("@sifra", aerodrom.Sifra));
		
				command.ExecuteNonQuery();


			}
			Data.Instance.UcitajAerodrome();

		}



		public void izmenaKorisnika(Korisnik korisnik)
		{
			using (SqlConnection conn = new SqlConnection())
			{
				conn.ConnectionString = Data.Instance.putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();


				command.CommandText = @"update korisnik set Gmail =@Gmail,Adresa=@Adresa,Ime=@Ime,KorisnickoIme=@KorisnickoIme ,Lozinka=@Lozinka,Pol=@Pol,Prezime=@Prezime,TipKorisnika=@TipKorisnika,Obrisan=@Obrisan where KorisnickoIme=@KorisnickoIme";

				

				command.Parameters.Add(new SqlParameter("@Adresa", korisnik.Adresa));
				command.Parameters.Add(new SqlParameter("@Gmail", korisnik.Gmail));
				command.Parameters.Add(new SqlParameter("@Ime", korisnik.Ime));
				command.Parameters.Add(new SqlParameter("@KorisnickoIme", korisnik.KorisnickoIme));
				command.Parameters.Add(new SqlParameter("@Lozinka", korisnik.Lozinka));
				command.Parameters.Add(new SqlParameter("@Pol", korisnik.Pol));
				command.Parameters.Add(new SqlParameter("@Prezime", korisnik.Prezime));
				command.Parameters.Add(new SqlParameter("@TipKorisnika", korisnik.TipKorisnika));
				command.Parameters.Add(new SqlParameter("@Obrisan", korisnik.obrisan));

				command.ExecuteNonQuery();
			}


		}

	 public void IzmeniLetove(Let a)
	{
			using (SqlConnection conn = new SqlConnection())
			{

				conn.ConnectionString = Data.Instance.putanja; 
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"update let set Obrisan=@Obrisan,CenaKarte=@CenaKarte,Destinacija=@Destinacija,Odrediste=@Odrediste,VremeDolaska=@VremeDolaska,VremePolaska=@VremePolaska  where BrojLeta=@BrojLeta ";
				


				command.Parameters.Add(new SqlParameter("@BrojLeta", a.BrojLeta));
				command.Parameters.Add(new SqlParameter("@CenaKarte", a.CenaKarte));
				command.Parameters.Add(new SqlParameter("@Destinacija", a.Destinacija));
				command.Parameters.Add(new SqlParameter("@Odrediste", a.Odrediste));
				command.Parameters.Add(new SqlParameter("@VremeDolaska", a.VremeDolaska));
				command.Parameters.Add(new SqlParameter("@VremePolaska", a.VremePolaska));
				command.Parameters.Add(new SqlParameter("@Obrisan", a.Obrisan));


				command.ExecuteNonQuery();
			}

			Data.Instance.UcitatisveLetove();
		}

		public void IzmenaKarte(karta a)
		{

			using (SqlConnection conn = new SqlConnection())
			{

				conn.ConnectionString = Data.Instance.putanja;

				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"update karta set cenakarte=@cenakarte,kapija=@kapija,klasaSedišta=@klasaSedišta,nazivPutnika=@nazivPutnika,obrisan=@obrisan where brojLeta=@brojLeta and  brojkolone=@brojkolone and brojreda=@brojreda";



				command.Parameters.Add(new SqlParameter("@brojLeta", a.brojLeta));
				command.Parameters.Add(new SqlParameter("@brojkolone", a.brojkolone));
				command.Parameters.Add(new SqlParameter("@cenakarte", a.cenakarte));
				command.Parameters.Add(new SqlParameter("@kapija", a.kapija));
				command.Parameters.Add(new SqlParameter("@brojreda", a.brojReda));
				command.Parameters.Add(new SqlParameter("@klasaSedišta", a.klasaSedišta));
				command.Parameters.Add(new SqlParameter("@nazivPutnika", a.nazivPutnika));
				command.Parameters.Add(new SqlParameter("@obrisan", a.obrisan));

				command.ExecuteNonQuery();
			}

			Data.Instance.ucitajKarte();
		}


		public void izmenaAvioKompanije(Aviokompanija aviokompanija)
		{
			using (SqlConnection conn = new SqlConnection())
			{
				conn.ConnectionString = Data.Instance.putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();


				command.CommandText = @"update aviokompanija set nazivAvioKompanije=@nazivAvioKompanije,obrisan=@obrisan where sifraAvioKompanije=@sifraAvioKompanije";

		
				command.Parameters.Add(new SqlParameter("@nazivAvioKompanije", aviokompanija.NazivAviokompanije));
				command.Parameters.Add(new SqlParameter("@obrisan", aviokompanija.obrisano));
				command.Parameters.Add(new SqlParameter("@sifraAvioKompanije", aviokompanija.Sifra));
				
				command.ExecuteNonQuery();
			}


		}
	}
	
}



