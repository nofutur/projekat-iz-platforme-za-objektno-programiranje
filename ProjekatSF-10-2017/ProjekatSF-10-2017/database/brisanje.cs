﻿using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatSF_10_2017.database
{
	class brisanje
	{
	


	private static brisanje _instance = null;


	public static brisanje Instance
	{
		get
		{
			if (_instance == null)

				_instance = new brisanje();
			return _instance;

		}
	}








		public void obrisiAvion(Avion avion)
		{
			using (SqlConnection conn = new SqlConnection())
			{
				conn.ConnectionString = Data.Instance.putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();


				command.CommandText = @"update avion set obrisan =@obrisan  where sifraAviona=@sifraAviona";

				
				command.Parameters.Add(new SqlParameter("@NazivAvioKompanije", avion.NazivAvioKompanije));
				command.Parameters.Add(new SqlParameter("@obrisan", true));
				command.Parameters.Add(new SqlParameter("@PilotIme", avion.PilotIme));
				command.Parameters.Add(new SqlParameter("@PilotPrezime", avion.PilotPrezime));
				command.Parameters.Add(new SqlParameter("@sifraAviona", avion.sifraAviona));
				command.Parameters.Add(new SqlParameter("@SifraEkonomskeKlase", avion.SifraEkonomskeKlase));
				command.Parameters.Add(new SqlParameter("@SifraBiznisKlase", avion.SifraBiznisKlase));
				command.ExecuteNonQuery();


			}

		
		}


	}

}
