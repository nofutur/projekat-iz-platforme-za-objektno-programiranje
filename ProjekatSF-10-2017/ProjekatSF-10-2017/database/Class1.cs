﻿using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Xceed.Wpf.Toolkit;

namespace ProjekatSF_10_2017.database
{



    class Data
    {
		public string putanja = "Data Source=(localdb)\\ProjectsV13;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
		public List<Let> letovi { get; set; }
        public List<Korisnik> Korisnici { get; set; }
        public List<Aerodrom> Aerodromi { get; set; }
        public List<Sediste> Sediste { get; set; }
        public String UlogovanKorisnik { get; set; }
        public List<Avion> avioni { get; set; }
		public List<karta> karte { get; set; }
		public List<Aviokompanija> aviokompanija { get; set; }
		
		public Data()
        {

			//UlogovanKorisnik = String.Empty;
			karte = new List<karta>();
			Korisnici = new List<Korisnik>();
            avioni = new List<Avion>();
            Aerodromi = new List<Aerodrom>();
            letovi = new List<Let>();
            Sediste = new List<Sediste>();
			aviokompanija = new List<Aviokompanija>();

			
        }

        private static Data _instance = null;


        public static Data Instance
        {
            get
            {
                if (_instance == null)

                    _instance = new Data();
                return _instance;

            }
        }



		public void SacuvajSveAerodrome(Aerodrom aerodrom)
		{


			using (SqlConnection conn = new SqlConnection())
			{
				conn.ConnectionString = Data.Instance.putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();


				command.CommandText = @"INSERT INTO aerodrom (Grad,Naziv,Obrisano,sifra)VALUES(@Grad,@Naziv,@Obrisano,@sifra)";

				command.Parameters.Add(new SqlParameter("@Grad", aerodrom.Grad));
				command.Parameters.Add(new SqlParameter("@Naziv", aerodrom.Naziv));
				command.Parameters.Add(new SqlParameter("@Obrisano", aerodrom.Obrisano));
				command.Parameters.Add(new SqlParameter("@sifra", aerodrom.Sifra));

				command.ExecuteNonQuery();


			}

			UcitajAerodrome();

		}

		public void UcitajAerodrome()
		{
			using (SqlConnection conn = new SqlConnection())
			{
				Aerodromi.Clear();
				conn.ConnectionString = putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"SELECT * FROM aerodrom";



				SqlDataAdapter daAerodromi = new SqlDataAdapter();

				DataSet dsAerodrom = new DataSet();
				daAerodromi.SelectCommand = command;
				daAerodromi.Fill(dsAerodrom, "aerodrom");


				foreach (DataRow row in dsAerodrom.Tables["aerodrom"].Rows)
				{
					Aerodrom a = new Aerodrom();

					a.Grad = (string)row["Grad"];
					a.Naziv = (string)row["Naziv"];
					a.Obrisano=(bool)row["Obrisano"];
					a.Sifra =Convert.ToInt16( row["sifra"]);
					Aerodromi.Add(a);

				}

			}

		}



		public void UcitajAvione()
			{
			using (SqlConnection conn = new SqlConnection())
			{
				avioni.Clear();
				conn.ConnectionString = putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"SELECT * FROM Avion";



				SqlDataAdapter daAvion = new SqlDataAdapter();

				DataSet dsAvion = new DataSet();
				daAvion.SelectCommand = command;
				daAvion.Fill(dsAvion, "Avion");


				foreach (DataRow row in dsAvion.Tables["Avion"].Rows)
				{
					Avion a = new Avion();
					
					a.SifraBiznisKlase = (string)row["SifraBiznisKlase"];
					a.SifraEkonomskeKlase = (string)row["SifraEkonomskeKlase"];
					a.PilotPrezime = (string)row["PilotPrezime"];
					a.PilotIme = (string)row["PilotIme"];
					a.NazivAvioKompanije  = (string)row["NazivAvioKompanije"];

					a.obrisan= (bool)row["obrisan"];
					Int16 b =(Int16)row["sifraAviona"];
					a.brojRedaBiznis = (string)row["redBiznis"];
					a.brojKolonaBiznis = (string)row["kolonaBiznis"];
					a.brojKolonaEkonomska = (string)row["kolonaEkonomska"];
					a.brojRedaEkonomska = (string)row["redEkonomska"];
					a.sifraAviona = Convert.ToString(b);
					avioni.Add(a);

				}

			}

		}

		public void SacuvajAvion(Avion a)
		{
			//praviti sa try
			using (SqlConnection conn = new SqlConnection())
			{

				conn.ConnectionString = putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"INSERT INTO Avion (sifraAviona,PilotIme,PilotPrezime,NazivAvioKompanije,SifraEkonomskeKlase,SifraBiznisKlase,obrisan,redBiznis,kolonaBiznis,kolonaEkonomska,redEkonomska)VALUES(@sifraAviona,@PilotIme,@PilotPrezime,@NazivAvioKompanije,@SifraEkonomskeKlase,@SifraBiznisKlase,@obrisan,@redBiznis,@kolonaBiznis,@kolonaEkonomska,@redEkonomska)";

			

				command.Parameters.Add(new SqlParameter("@sifraAviona", a.sifraAviona));
				command.Parameters.Add(new SqlParameter("@PilotIme", a.PilotIme));
				command.Parameters.Add(new SqlParameter("@PilotPrezime", a.PilotPrezime));
				command.Parameters.Add(new SqlParameter("@NazivAvioKompanije", a.NazivAvioKompanije));
				command.Parameters.Add(new SqlParameter("@SifraEkonomskeKlase", a.SifraEkonomskeKlase));
				command.Parameters.Add(new SqlParameter("@SifraBiznisKlase", a.SifraBiznisKlase));
				command.Parameters.Add(new SqlParameter("@obrisan", a.obrisan));
				command.Parameters.Add(new SqlParameter("@redBiznis", a.brojRedaBiznis));
				command.Parameters.Add(new SqlParameter("@kolonaBiznis", a.brojKolonaBiznis));
				command.Parameters.Add(new SqlParameter("@kolonaEkonomska", a.brojKolonaEkonomska));
				command.Parameters.Add(new SqlParameter("@redEkonomska", a.brojRedaEkonomska));
				command.ExecuteNonQuery();
			}

			UcitajAvione();
		}


		public void SacuvajAvioKomapniju(Aviokompanija a)
		{
			
			using (SqlConnection conn = new SqlConnection())
			{

				conn.ConnectionString = putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"INSERT INTO aviokompanija (sifraAvioKompanije,nazivAvioKompanije,obrisan)VALUES(@sifraAvioKompanije,@nazivAvioKompanije,@obrisan)";



				command.Parameters.Add(new SqlParameter("@sifraAvioKompanije", a.Sifra));
				command.Parameters.Add(new SqlParameter("@nazivAvioKompanije", a.NazivAviokompanije));
				command.Parameters.Add(new SqlParameter("@obrisan", a.obrisano));
				
				command.ExecuteNonQuery();
			}

			UcitajAvioKompaniju();
		}


		public void UcitajAvioKompaniju()
		{
			using (SqlConnection conn = new SqlConnection())
			{
				aviokompanija.Clear();
				conn.ConnectionString = putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"SELECT * FROM aviokompanija";



				SqlDataAdapter daAviokompanija = new SqlDataAdapter();

				DataSet dsAviokompanija = new DataSet();
				daAviokompanija.SelectCommand = command;
				daAviokompanija.Fill(dsAviokompanija, "aviokompanija");


				foreach (DataRow row in dsAviokompanija.Tables["aviokompanija"].Rows)
				{
					Aviokompanija a = new Aviokompanija();

					a.NazivAviokompanije = (string)row["nazivAvioKompanije"];
					a.obrisano =(bool)row["obrisan"];
					a.Sifra =Convert.ToInt16(row["sifraAvioKompanije"]);

					aviokompanija.Add(a);

				}

			}

		}


		public void SacuvajSedista(Sediste a)
		{

			using (SqlConnection conn = new SqlConnection())
			{

				conn.ConnectionString = putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"INSERT INTO Sediste (sifraAviona,BrojReda,BrojKolone,zauzetost,korisnickoIme,sifraklase,Obrisan,sifraLeta)VALUES(@sifraAviona,@BrojKolone,@BrojReda,@zauzetost,@korisnickoIme,@sifraklase,@Obrisan,@sifraLeta)";
				a.korisnickoIme = "nije-uneto";

				Console.WriteLine("+++++upis sedista"+a.SifraAviona);

				command.Parameters.Add(new SqlParameter("@BrojKolone", a.BrojKolone));
				command.Parameters.Add(new SqlParameter("@Obrisan", a.obrosan));
				command.Parameters.Add(new SqlParameter("@BrojReda", a.BrojReda));
				command.Parameters.Add(new SqlParameter("@zauzetost", a.zauzetost));
				command.Parameters.Add(new SqlParameter("@korisnickoIme", a.korisnickoIme));
				command.Parameters.Add(new SqlParameter("@sifraAviona", Convert.ToInt16(a.SifraAviona)));
				command.Parameters.Add(new SqlParameter("@sifraklase", Convert.ToInt16(a.sifraklase)));
				command.Parameters.Add(new SqlParameter("@sifraLeta", Convert.ToInt16(a.sifraLeta)));

				command.ExecuteNonQuery();
			}

			UcitajAvioKompaniju();
		}



		public void UcitajSvaSedista()
		{
			using (SqlConnection conn = new SqlConnection())
			{
				Sediste.Clear();
				conn.ConnectionString = putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"SELECT * FROM Sediste";



				SqlDataAdapter daSediste = new SqlDataAdapter();

				DataSet dsSediste = new DataSet();
				daSediste.SelectCommand = command;
				daSediste.Fill(dsSediste, "Sediste");


				foreach (DataRow row in dsSediste.Tables["Sediste"].Rows)
				{
					Sediste a = new Sediste();

					a.SifraAviona = (Int16)row["sifraAviona"];
					a.BrojReda = (Int16)row["BrojReda"];
					a.BrojKolone = (Int16)row["BrojKolone"];
					a.korisnickoIme = (string)row["korisnickoIme"];
					a.zauzetost = (bool)row["zauzetost"];
					a.obrosan= (bool)row["Obrisan"];
					a.sifraLeta= (string)row["sifraLeta"];
					a.sifraklase= (string)row["sifraklase"];
					Sediste.Add(a);

				}

			}

		}


		public void SacuvajKarte(karta a)
		{

			using (SqlConnection conn = new SqlConnection())
			{

				conn.ConnectionString = putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"INSERT INTO karta (brojLeta,brojkolone,cenakarte,kapija,brojReda,klasaSedišta,nazivPutnika,obrisan)VALUES(@brojLeta,@brojkolone,@cenakarte,@kapija,@brojReda,@klasaSedišta,@nazivPutnika,@obrisan)";
				
				 


				
				command.Parameters.Add(new SqlParameter("@brojLeta", a.brojLeta));
				command.Parameters.Add(new SqlParameter("@brojkolone", a.brojkolone));
				command.Parameters.Add(new SqlParameter("@cenakarte", a.cenakarte));
				command.Parameters.Add(new SqlParameter("@kapija", a.kapija));
				command.Parameters.Add(new SqlParameter("@brojReda", a.brojReda));
				command.Parameters.Add(new SqlParameter("@klasaSedišta", a.klasaSedišta));
				command.Parameters.Add(new SqlParameter("@nazivPutnika", a.nazivPutnika));
				command.Parameters.Add(new SqlParameter("@obrisan", a.obrisan));
				
				command.ExecuteNonQuery();
			}

			ucitajKarte();
		}

		public void ucitajKarte()
		{
			using (SqlConnection conn = new SqlConnection())
			{
				karte.Clear();
				conn.ConnectionString = putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"SELECT * FROM karta";



				SqlDataAdapter dakarta = new SqlDataAdapter();

				DataSet dskarta = new DataSet();
				dakarta.SelectCommand = command;
				dakarta.Fill(dskarta, "karta");


				foreach (DataRow row in dskarta.Tables["karta"].Rows)
				{
					karta a = new karta();

					a.brojkolone = (string)row["brojkolone"];
					a.cenakarte = Convert.ToDecimal(row["cenakarte"]);
					a.brojReda = (string)row["brojReda"];
					a.brojLeta = (string)row["brojLeta"];
					a.klasaSedišta = (string)row["klasaSedišta"];
					a.nazivPutnika = (string)row["nazivPutnika"]; 
					a.kapija = (string)row["kapija"];
					a.obrisan = (Boolean)row["Obrisan"];

					karte.Add(a);

				}

			}
		}

		public void UcitatisveLetove()
		{
			using (SqlConnection conn = new SqlConnection())
			{
				letovi.Clear();
				conn.ConnectionString = putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"SELECT * FROM let";



				SqlDataAdapter daLet = new SqlDataAdapter();

				DataSet dsLet = new DataSet();
				daLet.SelectCommand = command;
				daLet.Fill(dsLet, "let");


				foreach (DataRow row in dsLet.Tables["let"].Rows)
				{
					Let a = new Let();

					a.Obrisan = (Boolean)row["Obrisan"];
					a.BrojLeta = (string)row["BrojLeta"];
					a.CenaKarte =  Convert.ToDecimal(row["CenaKarte"]);
					a.Destinacija = (string)row["Destinacija"];
					a.Odrediste = (string)row["Odrediste"];
					a.VremeDolaska = (string)row["VremeDolaska"];
					a.VremePolaska = (string)row["VremePolaska"];

					letovi.Add(a);

				}

			}
		}


		public void SacuvajSveLetove(Let a)
		{
			using (SqlConnection conn = new SqlConnection())
			{

				conn.ConnectionString = putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"INSERT INTO let (BrojLeta,CenaKarte,Destinacija,Odrediste,VremeDolaska,VremePolaska,Obrisan)VALUES(@BrojLeta,@CenaKarte,@Destinacija,@Odrediste,@VremeDolaska,@VremePolaska,@Obrisan)";

				a.Obrisan = false;
				//Console.WriteLine("+++++" + a.SifraAviona);
				command.Parameters.Add(new SqlParameter("@BrojLeta", a.BrojLeta));
				command.Parameters.Add(new SqlParameter("@CenaKarte", a.CenaKarte));
				command.Parameters.Add(new SqlParameter("@Destinacija", a.Destinacija));
				command.Parameters.Add(new SqlParameter("@Odrediste", a.Odrediste));
				command.Parameters.Add(new SqlParameter("@VremeDolaska", a.VremeDolaska));
				command.Parameters.Add(new SqlParameter("@VremePolaska", a.VremePolaska));
				command.Parameters.Add(new SqlParameter("@Obrisan", a.Obrisan));
				command.ExecuteNonQuery();
			}

			UcitatisveLetove();
		}


		public void UcitajSveKorisnike()
		{
			using (SqlConnection conn = new SqlConnection())
			{
				Korisnici.Clear();
				conn.ConnectionString = putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"SELECT * FROM korisnik";

				SqlDataAdapter dakorisnik = new SqlDataAdapter();
				DataSet dskorisnik = new DataSet();
				dakorisnik.SelectCommand = command;
				dakorisnik.Fill(dskorisnik, "korisnik");


				foreach (DataRow row in dskorisnik.Tables["korisnik"].Rows)
				{
					Korisnik a = new Korisnik();
					
					a.Adresa = (string)row["Adresa"];
					a.Gmail =(string)row["Gmail"];
					a.Ime= (string)row["Ime"];
					a.Pol =(string)row["Pol"]; 
					a.Lozinka = (string)row["Lozinka"]; 
					a.TipKorisnika = (string)row["TipKorisnika"];
					a.KorisnickoIme = (string)row["KorisnickoIme"];
					a.Prezime= (string)row["Prezime"];
					a.obrisan = (Boolean)row["obrisan"];
					Korisnici.Add(a);

				}

			}
		}

		public void SacuvajSveKorisnike(Korisnik a)
		{
			using (SqlConnection conn = new SqlConnection())
			{

				conn.ConnectionString = putanja;
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				command.CommandText = @"INSERT INTO korisnik (Gmail,Ime,KorisnickoIme,Lozinka,Pol,Prezime,TipKorisnika,Adresa,obrisan)VALUES(@Gmail,@Ime,@KorisnickoIme,@Lozinka,@Pol,@Prezime,@TipKorisnika,@Adresa,@obrisan)";


			
				command.Parameters.Add(new SqlParameter("@Gmail", a.Gmail));
				command.Parameters.Add(new SqlParameter("@Ime", a.Ime));
				command.Parameters.Add(new SqlParameter("@KorisnickoIme", a.KorisnickoIme));
				command.Parameters.Add(new SqlParameter("@Lozinka", a.Lozinka));
				command.Parameters.Add(new SqlParameter("@Pol", a.Pol));
				command.Parameters.Add(new SqlParameter("@Prezime", a.Prezime)); 
				command.Parameters.Add(new SqlParameter("@TipKorisnika", a.TipKorisnika));
				command.Parameters.Add(new SqlParameter("@Adresa", a.Adresa));
				command.Parameters.Add(new SqlParameter("@obrisan", a.obrisan));
				command.ExecuteNonQuery();
			}

			UcitajSveKorisnike(); 
		}


	}



}

