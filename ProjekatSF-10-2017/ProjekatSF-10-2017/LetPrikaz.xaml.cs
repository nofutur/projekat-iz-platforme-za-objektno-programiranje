﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ProjekatSF_10_2017.LetPrikazi1;
using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
namespace ProjekatSF_10_2017
{
	/// <summary>
	/// Interaction logic for LetPrikaz.xaml
	/// </summary>
	public partial class LetPrikaz : Window
	{

		List<Let> LetLista = new List<Let>();
		public LetPrikaz()
		{

			InitializeComponent();
			PrikazLetova.ItemsSource = LetLista;
			Reload();

			PrikazLetova.IsSynchronizedWithCurrentItem = true;
			PrikazLetova.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);


		}




		public void Reload()
		{
			LetLista.Clear();
			Data.Instance.UcitatisveLetove();
			foreach (var let in Data.Instance.letovi)
			{
				if (let.Obrisan == false)
					LetLista.Add(let);

			}
			PrikazLetova.Items.Refresh();
		}





		private void BtnDodaj_Click_1(object sender, RoutedEventArgs e)
		{


			UnosNovogLeta a = new UnosNovogLeta();
			a.Show();
			this.Close();

		}




		private void BtnIzmeni_Click_1(object sender, RoutedEventArgs e)
		{


			if (SelektovanAerodrom((Let)PrikazLetova.SelectedItem) == true)
			{
				PregledPojedinacnihLetova1 a = new PregledPojedinacnihLetova1((Let)PrikazLetova.SelectedItem, "izmena");
				a.Show();
			}
			

		}

	

		private void btnPojedinacniPrikaz_Click(object sender, RoutedEventArgs e)
		{
			if (SelektovanAerodrom((Let)PrikazLetova.SelectedItem) == true)
			{

				PregledPojedinacnihLetova1 a = new PregledPojedinacnihLetova1((Let)PrikazLetova.SelectedItem, "pregled");
				a.Show();

			}
			
			
		}

		private void BtnObrisi_Click_1(object sender, RoutedEventArgs e)
		{

			Let let = (Let)PrikazLetova.SelectedValue;
			LetLista.Remove(let);
			let.Obrisan = true;
			izmena.Instance.IzmeniLetove(let);

			Reload();
			PrikazLetova.Items.Refresh();


		}

	
		private bool SelektovanAerodrom(Let let)
		{
			if (let == null)
			{
				MessageBox.Show("Nije selektovan let");
				return false;

			}

			return true;
		}

		private void Brojleta_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (brojleta.Text == "")
			{
				Reload();
			}
			else
			{

				
				LetLista.Clear();
				foreach (Let let in Data.Instance.letovi)
					if (let.BrojLeta.Contains(brojleta.Text))
						LetLista.Add(let);

				PrikazLetova.Items.Refresh();
			}
		}



		private void AvioKompanija_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (odrediste.Text == "")
			{
				Reload();
			}
			else
			{

				
				LetLista.Clear();
				foreach (Let let in Data.Instance.letovi)
					if (let.Odrediste.Contains( odrediste.Text))
						if (let.Obrisan == false)
							LetLista.Add(let);

				PrikazLetova.Items.Refresh();
			}
		}

		private void Destinacija_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (destinacija.Text == "")
			{
				Reload();
			}
			else
			{


				LetLista.Clear();
				foreach (Let let in Data.Instance.letovi)
					if (let.Destinacija.Contains(destinacija.Text))
						if(let.Obrisan==false)
						LetLista.Add(let);

				PrikazLetova.Items.Refresh();
			}

		}

		private void Cena_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (destinacija.Text == "")
			{
				Reload();
			}
			else
			{

				LetLista.Clear();
				foreach (Let let in Data.Instance.letovi)
					if (let.Destinacija.Contains(destinacija.Text))
						if (let.Obrisan == false)
							LetLista.Add(let);

				PrikazLetova.Items.Refresh();
			}

		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (cenaMin.Text == "")
			{
				Reload();
			}
			else
			{

				LetLista.Clear();
				foreach (Let let in Data.Instance.letovi)
					if (let.Destinacija.Contains(destinacija.Text))
						if (let.Obrisan == false)
							LetLista.Add(let);

				PrikazLetova.Items.Refresh();
			}




		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			if (cenaMin.Text == "" && cenaMin.Text=="")
			{
				Reload();
			}
			else
			{

				LetLista.Clear();
				foreach (Let let in Data.Instance.letovi)
					if (Convert.ToDecimal(cenaMin.Text) < Convert.ToDecimal(let.CenaKarte) && Convert.ToDecimal(let.CenaKarte ) < Convert.ToDecimal(cenaMax.Text))
						if (let.Obrisan == false)
							LetLista.Add(let);

				PrikazLetova.Items.Refresh();
			}
		}

		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			
			

			LetLista.Clear();
			foreach (Let let in Data.Instance.letovi)
				if (Convert.ToDateTime(unosVremePolaska.Text) < Convert.ToDateTime( let.VremePolaska) && Convert.ToDateTime(let.VremePolaska )< Convert.ToDateTime(unosVremedolaska.Text))
					if (let.Obrisan == false)
						LetLista.Add(let);

			PrikazLetova.Items.Refresh();
			
		}

		private void Button_Click_2(object sender, RoutedEventArgs e)
		{
			Reload();
		}

		private void Button_Click_3(object sender, RoutedEventArgs e)
		{
			Reload();
		}

		private void Button_Click_4(object sender, RoutedEventArgs e)
		{
			LetLista.Clear();
			foreach (Let let in Data.Instance.letovi)
				if (Convert.ToDateTime(mindolazak.Text) < Convert.ToDateTime(let.VremeDolaska) && Convert.ToDateTime(let.VremeDolaska) < Convert.ToDateTime(maxdolazak.Text))
					if (let.Obrisan == false)
						LetLista.Add(let);

			PrikazLetova.Items.Refresh();
		}

		private void Button_Click_5(object sender, RoutedEventArgs e)
		{
			Reload();
		}

		private void CenaMin_TextChanged(object sender, TextChangedEventArgs e)
		{

		}
	}
}
