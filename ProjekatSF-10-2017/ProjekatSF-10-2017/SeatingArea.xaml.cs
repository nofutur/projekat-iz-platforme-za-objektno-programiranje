﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{

	public partial class SeatingArea : Window
	{
		public ObservableCollection<int> Seats { get; private set; }

		public SeatingArea()
		{
			Seats = new ObservableCollection<int>();
			for (int i =10 ; i <= 200; i++)

			Seats.Add(i);

			InitializeComponent();
		}
	}
}