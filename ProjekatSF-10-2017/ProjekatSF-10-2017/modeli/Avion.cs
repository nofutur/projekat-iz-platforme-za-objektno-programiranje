﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatSF_10_2017.modeli
{
     public class Avion
    {

        public Avion() { }

        public Avion(string pilotIme,String sifra, string pilotPrezime, string nazivAvioKompanije, string sifraEkonomskeKlase, string sifraBiznisKlase, bool obrisan,String brojReda,string brojKolona ,String brojKolonaEkonomskaa,String brojRedaEkonomskaa)
        {
            PilotIme = pilotIme;
            PilotPrezime = pilotPrezime;
         
			sifraAviona = sifra;
			NazivAvioKompanije = nazivAvioKompanije;
            SifraEkonomskeKlase = sifraEkonomskeKlase;
            SifraBiznisKlase = sifraBiznisKlase;
            this.obrisan = obrisan;
			brojKolonaBiznis = brojKolona;
			brojRedaBiznis = brojReda;
			brojKolonaEkonomska = brojKolonaEkonomskaa;
			brojRedaEkonomska= brojRedaEkonomskaa;
		}


		public String sifraAviona { get; set; }
		public String PilotIme { get; set; }
        public String PilotPrezime { get; set; }
        public String NazivAvioKompanije { get; set; }
        public String SifraEkonomskeKlase { get; set; }
        public String SifraBiznisKlase { get; set; }
		public String brojKolonaBiznis { get; set; }
		public String brojRedaBiznis { get; set; }
		public String brojKolonaEkonomska { get; set; }
		public String brojRedaEkonomska { get; set; }



		public bool obrisan { get; set; }

		public override string ToString()
		{
	
			return $"PilotIme {PilotIme} Prezime {PilotPrezime}  sifraAviona {sifraAviona} NazivAvioKompanije {NazivAvioKompanije} SifraEkonomskeKlase{SifraEkonomskeKlase} SifraBiznisKlase {SifraBiznisKlase} obrisan {obrisan} ";
		}



	}
}
