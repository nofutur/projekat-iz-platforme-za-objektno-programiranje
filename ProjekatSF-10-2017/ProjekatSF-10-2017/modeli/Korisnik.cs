﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatSF_10_2017.modeli
{
    
      public   class Korisnik
        {
            public String Ime { get; set; }
            public String Prezime { get; set; }
            public String Lozinka { get; set; }
            public String KorisnickoIme { get; set; }
            public string Pol { get; set; }
            public String Adresa { get; set; }
            public String Gmail { get; set; }
			public string TipKorisnika { get; set; }
			public Boolean obrisan { get; set; }





		public Korisnik() { }

		public Korisnik(Boolean obrisan, String Ime, String Prezime, String Lozinka, String KorisnickoIme, String Pol, String Adresa, String TipKorisnika,String gmail)

            {
                this.Ime = Ime;
                this.Prezime = Prezime;
                this.Lozinka = Lozinka;
                this.KorisnickoIme = KorisnickoIme;
                this.Pol = Pol;
                this.Gmail = gmail;
                this.Adresa = Adresa;
                this.TipKorisnika = TipKorisnika;
				this.obrisan = obrisan;
            }

            public override string ToString()
            {
                //return "Ime " + Ime;
                return $"Ime {Ime} Prezime {Prezime} Lozinka {Lozinka} KorisnickoIme {KorisnickoIme} gmail{Gmail} Pol {Pol} Adresa {Adresa} Tip Korisnika {TipKorisnika}";
            }


        }
    }

