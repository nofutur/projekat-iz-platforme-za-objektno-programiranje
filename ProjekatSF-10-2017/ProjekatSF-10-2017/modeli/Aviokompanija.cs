﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatSF_10_2017.modeli
{
    public class Aviokompanija
    {

        public Int16 Sifra { get; set; }
        public String  NazivAviokompanije { get; set; }
       
		public bool obrisano { get; set; }


		public Aviokompanija() { }


		public Aviokompanija(Int16 sifra , String nazivaviokompanije ,  bool obrisano)
        {

            //this.let = lett;
            this.NazivAviokompanije = nazivaviokompanije;
            this.Sifra = sifra;
			this.obrisano = obrisano;

		}

		public override string ToString()
		{

			return $"Sifra {Sifra} NazivAviokompanije {NazivAviokompanije}  obrisano {obrisano} ";
		}


	}
}
