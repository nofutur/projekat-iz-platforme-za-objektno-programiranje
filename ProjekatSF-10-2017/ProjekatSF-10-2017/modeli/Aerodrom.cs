﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatSF_10_2017.modeli
{
    public class Aerodrom
    {
        public Int16 Sifra { get; set; }
        public String Naziv { get; set; }
        public String Grad { get; set; }
        public Boolean Obrisano { get; set; }

        public Aerodrom() { }

        public Aerodrom(Int16 sifra, String naziv, String grad, Boolean obrisano)
        {
            this.Grad = Grad;
            this.Naziv = naziv;
            this.Sifra = sifra;
            this.Obrisano = obrisano;


        }

        public override string ToString()
        {
            return $"sifra {Sifra} Naziv {Naziv} Grad {Grad}";
        }
    }
}
