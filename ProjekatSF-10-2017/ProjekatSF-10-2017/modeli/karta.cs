﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatSF_10_2017.modeli
{
	public class karta
	{




		public  karta() { }



		public karta(Boolean obrisan, string brojLeta, string brojkolone, string nazivPutnika, string kapija, string klasaSedišta, Decimal cenakarte, string brojRedaa)
		{
			this.obrisan = obrisan;
			this.brojLeta = brojLeta;
			this.brojkolone = brojkolone;
			this.brojReda = brojRedaa;
			this.nazivPutnika = nazivPutnika;
			this.kapija = kapija;
			this.klasaSedišta = klasaSedišta;
			this.cenakarte = cenakarte;
			
		}

		public String brojLeta { get; set; }
		public String brojkolone { get; set; }
		public String nazivPutnika { get; set; }
		public String kapija { get; set; }
		public String klasaSedišta { get; set; }
		public Decimal cenakarte { get; set; }
		public String brojReda { get; set; }
		public Boolean obrisan { get; set; }


		public override string ToString()
		{
			
			return $"brojLeta {brojLeta} brojReda {brojReda}  brojkolone {brojkolone} nazivPutnika {nazivPutnika} kapija {kapija} klasaSedišta{klasaSedišta} cenakarte {cenakarte} ";
		}



	}
}
