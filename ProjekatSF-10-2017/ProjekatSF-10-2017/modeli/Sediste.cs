﻿using ProjekatSF_10_2017.database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjekatSF_10_2017.modeli
{
    public class Sediste
	{
		public Sediste() { }


		public Sediste( int brojKolone, int brojReda, int SifraAvionaa)
		{
			zauzetost = true;
			BrojKolone = brojKolone;
			BrojReda = brojReda;
			SifraAviona = SifraAvionaa;
		}
		public Sediste(ESediste zauzetost, string sifraklase, int brojKolone, int brojReda, int SifraAvionaa)
		{
			zauzetost = zauzetost;
			BrojKolone = brojKolone;
			BrojReda = brojReda;
			SifraAviona = SifraAvionaa;
			sifraklase = sifraklase;
		}

		public Sediste(ESediste zauzetost, string sifraklase, int brojKolone, int brojReda, int SifraAvionaa,string korisnickoIme,string sifraLeta,Boolean obrosann)
		{
			zauzetost = zauzetost;
			BrojKolone = brojKolone;
			BrojReda = brojReda;
			SifraAviona = SifraAvionaa;
			korisnickoIme = korisnickoIme;
			sifraLeta = sifraLeta;
			sifraklase = sifraklase;
			obrosan = obrosann;
		}
		public bool obrosan { get; set; }
		public bool zauzetost { get; set; }
        public int BrojKolone { get; set; }
        public int BrojReda { get; set; }
        public int SifraAviona { get; set; }
		public string sifraLeta { get; set; }
		public String korisnickoIme { get; set; }
		public String sifraklase { get; set; }


		public override string ToString()
	{

		return $"zauzetost {zauzetost} BrojKolone {BrojKolone}  BrojReda {BrojReda}  SifraLeta {SifraAviona} ";
	}

		internal void stvaranjeSedista(string text)
		{
			throw new NotImplementedException();
		}




		public void stvaranjeSedista(int kolona,int red ,int sifraAviona, string sifraklase,string sifravionaLeta)
		{

			
			for (int i = 1; i < kolona; i++)
			{
				for(int j = 1; j < red; j++)
				{

					Sediste s = new Sediste();
					s.BrojKolone = i;
					s.BrojReda = j;
					s.korisnickoIme = "nije-uneto";
					s.zauzetost = false;
					s.sifraklase = sifraklase;
					s.obrosan = false;
					s.SifraAviona = sifraAviona;
					s.sifraLeta = sifravionaLeta;
					Data.Instance.SacuvajSedista(s);
				}
			}
		}

	}

}
