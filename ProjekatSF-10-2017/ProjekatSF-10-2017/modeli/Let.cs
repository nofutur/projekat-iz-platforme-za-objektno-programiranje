﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Wpf.Toolkit;

namespace ProjekatSF_10_2017.modeli
{
    public class   Let
    {
        //internal string destinacija;

        public String BrojLeta { get; set; }
        public String VremePolaska { get; set; }
        public String VremeDolaska { get; set; }
        public String Odrediste { get; set; }
        public String Destinacija { get; set; }
        public Decimal CenaKarte { get; set; }
        public Boolean Obrisan { get; set; }

		public Let(Boolean Obrisann, string brojLeta, String vremePolaska, String vremeDolaska,  string odrediste, string destinacija, decimal cenaKarte)
        {
            BrojLeta = brojLeta;
            VremePolaska = vremePolaska;
            VremeDolaska = vremeDolaska;
            Odrediste = odrediste;
            Destinacija = destinacija;
            CenaKarte = cenaKarte;
			Obrisan = Obrisann;
		}

        public Let()
        { }
        public override string ToString()
        {
            return " Broj leta  - " + BrojLeta + "  Vreme Polaska  - " + VremePolaska + "  Vreme Dolaska -  " + VremeDolaska + "  Odrediste -   " + Odrediste + "   Destinacija -   " + "  CenaKarte  -   " + CenaKarte + "  Obrisan  -   " + Obrisan;
        }
    }
}
