﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
           
			Data.Instance.UcitajAvioKompaniju();
			Data d = new Data();
            d.UcitajSveKorisnike();
            d.UcitajAerodrome();
            //d.SacuvajSvaSedista();
            d.UcitatisveLetove();

           

           


        }

        private void btnPrikazAerodroma_Click(object sender, RoutedEventArgs e)
        {
            AerodromMeni aa = new AerodromMeni();
            aa.Show();
            
        }

      

        private void Registracija_Click(object sender, RoutedEventArgs e)
        {

			Let a = new Let();
			a = null;
            Registracija prikazRegistracije = new Registracija(a,"meni",null);
            prikazRegistracije.Show();
        }



     



        private void Prijavaorisnika_Click(object sender, RoutedEventArgs e)
        {
            Prijavljivanje p = new Prijavljivanje();
            p.Show();
        }



        private void NoviLet_Click(object sender, RoutedEventArgs e)
        {
            UnosNovogLeta u = new UnosNovogLeta();
            u.Show();

        }

        private void Pretragaletova_Click(object sender, RoutedEventArgs e)
        {
			PregledLetovaa aa = new PregledLetovaa("korisnik");
            aa.Show();
        }

        

        private void StvaranjeAviona_Click(object sender, RoutedEventArgs e)
        {
            Avionn a = new Avionn();
            a.Show();
        }

		private void Aviokompanija_Click(object sender, RoutedEventArgs e)
		{
			Avio_kompanija a = new Avio_kompanija();
			a.Show();
		}

		private void Stvaranjesedista_Click(object sender, RoutedEventArgs e)
		{
			StvaranjeSedista a = new StvaranjeSedista();
			a.Show();
		}

		private void Karta_Click(object sender, RoutedEventArgs e)
		{
			Karta k = new Karta();
			k.Show();

		}
	}
}
