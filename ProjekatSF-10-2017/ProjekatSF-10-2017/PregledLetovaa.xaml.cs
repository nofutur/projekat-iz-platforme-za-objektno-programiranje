﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
	/// <summary>
	/// Interaction logic for PregledLetovaa.xaml
	/// </summary>
	public partial class PregledLetovaa : Window
	{

		public string odakle;


		ICollectionView prikaz;


		List<Let> letovi1 = new List<Let>();


		public PregledLetovaa(string od)
		{

			this.odakle = od;


			InitializeComponent();

			Reload();
			Data.Instance.UcitatisveLetove();

			LetoviPrikaz.ItemsSource = letovi1;
			LetoviPrikaz.IsSynchronizedWithCurrentItem = true;
			LetoviPrikaz.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

			
				
			

		}





		private void unos(List<Let> a)
		{


			LetoviPrikaz.ItemsSource = a;
			LetoviPrikaz.IsSynchronizedWithCurrentItem = true;
			LetoviPrikaz.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);


		}

		private Let prikai()
		{
			foreach (var let in letovi1)


				if (let.Destinacija.Equals(Destinacija.Text))


					return let;



			return null;





		}

		private void Odrediste_TextChanged(object sender, TextChangedEventArgs e)
		{
			
			if (Odrediste.Text == "")
			{
				Reload();
			}
			else
			{

				letovi1.Clear();
				foreach (Let let in Data.Instance.letovi)
					if (let.Odrediste.Contains(Odrediste.Text))
						if (let.Obrisan == false)
							letovi1.Add(let);

				LetoviPrikaz.Items.Refresh();
			}

		}



		private void Destinacija_TextChanged(object sender, TextChangedEventArgs e)
		{


			if (Destinacija.Text == "")
			{
				Reload();
			}
			else
			{


				letovi1.Clear();
				foreach (Let let in Data.Instance.letovi)
					if (let.Destinacija.Contains(Destinacija.Text))
						if (let.Obrisan == false)
							letovi1.Add(let);

			}


				LetoviPrikaz.Items.Refresh();


		}






		private void Dalje_Click_1(object sender, RoutedEventArgs e)
		{

			if (LetoviPrikaz.SelectedValue == null)
			{
				MessageBox.Show("Odaberi let");
			}
			else
			{


				if (odakle == "korisnik")
				{
				
					Let a = (Let)LetoviPrikaz.SelectedValue;
					Console.WriteLine(a);
					ProveraUlogovanje prov = new ProveraUlogovanje(a);


					prov.Show();
					this.Close();

				}

				if (odakle == "administrator")
				{
					Let a = (Let)LetoviPrikaz.SelectedValue;
					OdabirKorisnika c = new OdabirKorisnika(a);
					c.Show();
					this.Close();
				}

				if (odakle == "meni")
				{
					Close();




				}

			}

		}
		public void Reload()
		{
			letovi1.Clear();
			Data.Instance.UcitatisveLetove();
			foreach (var let in Data.Instance.letovi)
			{
				if (let.Obrisan == false)
					letovi1.Add(let);

			}

			LetoviPrikaz.Items.Refresh();
		}


		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (brojleta.Text == "")
			{
				Reload();
			}
			else
			{


				letovi1.Clear();
				foreach (Let let in Data.Instance.letovi)
					if (let.BrojLeta.Contains(brojleta.Text))
						if (let.Obrisan == false)
							letovi1.Add(let);


			}

            LetoviPrikaz.Items.Refresh();
		}

		

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			if (cenaMin.Text == "" && cenaMax.Text == "")
			{
				Reload();
			}
			else
			{

				letovi1.Clear();
				foreach (Let let in Data.Instance.letovi)
					if (Convert.ToDecimal(cenaMin.Text) < Convert.ToDecimal(let.CenaKarte) && Convert.ToDecimal(let.CenaKarte) < Convert.ToDecimal(cenaMax.Text))
						if (let.Obrisan == false)
							letovi1.Add(let);

				
			}

			LetoviPrikaz.Items.Refresh();
		}

		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			Reload();
			brojleta.Text = "";
			cenaMin.Text = "";
			cenaMax.Text = "";
			Destinacija.Text ="";
			Odrediste.Text = "";
		}

		private void Button_Click_2(object sender, RoutedEventArgs e)
		{
			letovi1.Clear();
			foreach (Let let in Data.Instance.letovi)
				if (Convert.ToDateTime(unosVremePolaskaPocetak.Text) < Convert.ToDateTime(let.VremePolaska) && Convert.ToDateTime(let.VremePolaska) < Convert.ToDateTime(unosVremePolaskaKraj.Text))
					if (let.Obrisan == false)
						letovi1.Add(let);

			LetoviPrikaz.Items.Refresh();
		}

		private void Button_Click_3(object sender, RoutedEventArgs e)
		{
			letovi1.Clear();
			foreach (Let let in Data.Instance.letovi)
				if (Convert.ToDateTime(unosVremeSletanjaPocetak.Text) < Convert.ToDateTime(let.VremePolaska) && Convert.ToDateTime(let.VremePolaska) < Convert.ToDateTime(unosVremeSletanjaKraj.Text))
					if (let.Obrisan == false)
						letovi1.Add(let);

			LetoviPrikaz.Items.Refresh();
		}
	}
}