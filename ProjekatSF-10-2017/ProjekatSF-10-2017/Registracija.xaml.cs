﻿using ProjekatSF_10_2017.database;

using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace ProjekatSF_10_2017
{

	
   
    public partial class Registracija : Window


	{
		String propust;
		Let let;
		Korisnik korisnik;

		



		public Registracija(Let let ,String a,Korisnik ko)
        {

			
			

			propust = a;
			korisnik = ko;
			InitializeComponent();
			PolComboBox.Items.Add("M");
			PolComboBox.Items.Add("Z");
		
			this.let = let;

			if (propust == "meni")
			{

				izmena.IsEnabled = false;

			}



			if (propust=="izmena"){


				BtnSacuvaj.IsEnabled = false;

				UnosKorisnickoIme.Text = korisnik.KorisnickoIme;
				UosIme.Text = korisnik.Ime;
				unosPrezime.Text = korisnik.Prezime;
				UnosAdreseStanovanja.Text = korisnik.Adresa;
				UnosEmail.Text = korisnik.Gmail;

		
			}






			if (propust=="pregled")
			{
				izmena.IsEnabled = false;
				BtnSacuvaj.IsEnabled = false;
				
				UnosKorisnickoIme.Text = korisnik.KorisnickoIme;
				UosIme.Text = korisnik.Ime;
				unosPrezime.Text = korisnik.Prezime;
				UnosAdreseStanovanja.Text = korisnik.Adresa;
				UnosEmail.Text = korisnik.Gmail;

				UnosKorisnickoIme.IsEnabled = false;
				UosIme.IsEnabled = false;
				unosPrezime.IsEnabled = false;
				UnosAdreseStanovanja.IsEnabled = false;
				UnosEmail.IsEnabled = false;
			}

		}



		


		private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
		{

			if (UnosKorisnickoIme.Text != "" && UosIme.Text != "" && unosPrezime.Text != "" && UnosAdreseStanovanja.Text != "" && Lozinka.Password != ""&& PolComboBox.Text !="")
			{

				Korisnik korisnik = new Korisnik();
				korisnik.KorisnickoIme = UnosKorisnickoIme.Text;
				korisnik.Ime = UosIme.Text;
				korisnik.Prezime = unosPrezime.Text;
				korisnik.Adresa = UnosAdreseStanovanja.Text;
				korisnik.TipKorisnika = Convert.ToString( ETipKorisnika.PUTNIK);
				korisnik.Gmail = UnosEmail.Text;
				korisnik.Pol = PolComboBox.Text;

				if (ProveraLozinke.Password == Lozinka.Password)
				{
					korisnik.Lozinka = Lozinka.Password;

					Data a = new Data();
					a.UcitajSveKorisnike();
					
					a.SacuvajSveKorisnike(korisnik);

				if (propust=="letovi")
				{
					ProveraUlogovanje b = new ProveraUlogovanje(let);
					b.Show();
					this.Close();
				}
				else if (propust == "meni")
				{
					this.Close();
				}

				}
				else MessageBox.Show("Lozinka je pogresna");

			}

		else MessageBox.Show("Niste uneli sve podatke");

		}
				





		private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();

        }

        private void UosIme_TextChanged(object sender, TextChangedEventArgs e)
        {




        }

		

		private void Izmena_Click_1(object sender, RoutedEventArgs e)
		{
			Korisnik kor = new Korisnik();



				kor.TipKorisnika = korisnik.TipKorisnika;
				kor.Pol= korisnik.Pol;
				kor.Lozinka = korisnik.Lozinka;
				kor.Adresa = UnosAdreseStanovanja.Text;
				kor.Gmail = UnosEmail.Text;
				kor.KorisnickoIme = UnosKorisnickoIme.Text;
				kor.Prezime = unosPrezime.Text;
				kor.Ime = UosIme.Text;
				kor.obrisan = false;
				database.izmena.Instance.izmenaKorisnika(kor);
		}

		private void PolComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{

		}
	}
}