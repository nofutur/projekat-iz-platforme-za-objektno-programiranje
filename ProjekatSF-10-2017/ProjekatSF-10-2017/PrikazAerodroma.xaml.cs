﻿using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
    /// <summary>
    /// Interaction logic for PrikazAerodroma.xaml
    /// </summary>
    public partial class PrikazAerodroma : Window
    {
        public PrikazAerodroma(Aerodrom aerodrom)
        {
            InitializeComponent();

            kutija1.Text= Convert.ToString(aerodrom.Sifra);
            kutija2.Text = aerodrom.Grad;
            kutija3.Text = aerodrom.Naziv;

            kutija3.IsEnabled = false;
            kutija2.IsEnabled = false;
            kutija1.IsEnabled = false;



        }

        private void kutija1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void kutija2_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void kutija3_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}