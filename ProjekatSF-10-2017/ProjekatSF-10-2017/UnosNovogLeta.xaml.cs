﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace ProjekatSF_10_2017
{
    /// <summary>
    /// Interaction logic for UnosNovogLeta.xaml
    /// </summary>
    public partial class UnosNovogLeta : Window
    {


        

        public UnosNovogLeta()
        {
            InitializeComponent();
            


			Data.Instance.UcitajAerodrome();
			polazakAerodromi();

			Data.Instance.UcitajAvione();
			foreach (var avion in Data.Instance.avioni)
				obabirAviona.Items.Add(avion.sifraAviona);

		}



		public void polazakAerodromi()
        {     


            if (OdabirMestaPolaska.HasItems)
                OdabirMestaPolaska.Items.Clear();
            foreach (var aerodrom1 in Data.Instance.Aerodromi)
            {
                if (!aerodrom1.Obrisano)
                {
                    OdabirMestaPolaska.Items.Add(aerodrom1);
                    
                }
            }



        }

        public void dolazakAerodromi()
        {      

            
            if (OdabirMestaDolaska.HasItems)
                OdabirMestaDolaska.Items.Clear();
            foreach (var aerodrom2 in Data.Instance.Aerodromi)
            {
                if (!aerodrom2.Obrisano)
                {
                    OdabirMestaDolaska.Items.Add(aerodrom2);
                   
                }
            }
        }






        private bool SelektovanAerodrom(Aerodrom aerodrom)
        {
            if (aerodrom == null)
            {
                System.Windows.MessageBox.Show("Nije selektovan aerodrom");
                return false;
            }

            return true;
        }


        

        private void BtnDodajLet_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom aerodrom = (Aerodrom)OdabirMestaPolaska.SelectedItem;
            

            Aerodrom aerodrom1 = (Aerodrom)OdabirMestaDolaska.SelectedItem;

            if(Double.Parse(cenaUnos.Text)<0) System.Windows.MessageBox.Show("cena karte ne moze biti negativna");


			if ((Aerodrom)OdabirMestaPolaska.SelectedItem != null && (Aerodrom)OdabirMestaDolaska.SelectedItem != null && unosVremedolaska.Text != "" && unosVremePolaska.Text != "" && cenaUnos.Text != null)
			{



				Let let = new Let();


				let.BrojLeta = StvaranjeIproveraBrojaLeta();
				let.Destinacija = aerodrom.Grad;
				let.Odrediste = aerodrom1.Grad;
				let.VremeDolaska = unosVremedolaska.Text;
				let.VremePolaska = unosVremePolaska.Text;
				let.CenaKarte = Convert.ToDecimal(cenaUnos.Text);
				let.Obrisan = false;




				Data.Instance.UcitajAvione();
				foreach (var avion in Data.Instance.avioni)

				{
				Console.WriteLine(obabirAviona.Text);
				Console.WriteLine(avion.sifraAviona);
					if (obabirAviona.Text.Equals(avion.sifraAviona))
					{
						Sediste C = new Sediste();
						Console.WriteLine("stvaranje sedista 000000");
						C.stvaranjeSedista(Convert.ToInt16(avion.brojKolonaBiznis), Convert.ToInt16(avion.brojRedaBiznis), Convert.ToInt16(avion.sifraAviona),  avion.SifraBiznisKlase, let.BrojLeta);
						C.stvaranjeSedista(Convert.ToInt16(avion.brojKolonaEkonomska), Convert.ToInt16(avion.brojRedaEkonomska), Convert.ToInt16(avion.sifraAviona),  avion.SifraEkonomskeKlase, let.BrojLeta);
						
					}

	
				}
				Data.Instance.SacuvajSveLetove(let);
				LetPrikaz a = new LetPrikaz();
				a.Show();

				this.Close();


			}
            else System.Windows.MessageBox.Show("Niste uneli sve podatke");


		}


        public String StvaranjeIproveraBrojaLeta()
        {
            int a;
            Boolean c = true;
            while (c) {
                Random rnd = new Random();
                a = rnd.Next(99, 1000);
                c = false;
                foreach (var let in Data.Instance.letovi)
                    if (int.Parse(let.BrojLeta) == a)
                    {
                        StvaranjeIproveraBrojaLeta();

                        c = true;
                        break;
                    }

                if (c!=true){ c = false; return Convert.ToString(a); }

               
            }
            return "";          
        }



        private void OdabirMestaPolaska_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dolazakAerodromi();
        }

        private void OdabirMestaDolaska_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

		private void ObabirAviona_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{

		}
	}
    }
