﻿using ProjekatSF_10_2017.database;
using ProjekatSF_10_2017.modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjekatSF_10_2017
{
	/// <summary>
	/// Interaction logic for KorisnikGlavno.xaml
	/// </summary>
	public partial class KorisnikGlavno : Window
	{
		List<Korisnik> KorisnikList = new List<Korisnik>();
		List<Korisnik> KorisnikList1 = new List<Korisnik>();

		public KorisnikGlavno()
		{
			InitializeComponent();
			Reload();
			PrikazKorisnika.ItemsSource = KorisnikList;

		
			PrikazKorisnika.IsSynchronizedWithCurrentItem = true;
			PrikazKorisnika.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);




			uloga.Items.Add("");
			uloga.Items.Add("PUTNIK");
			uloga.Items.Add("ADMINISTRATOR");


		}








		public void Reload()
		{


			Data.Instance.UcitajSveKorisnike();
			KorisnikList.Clear();
			foreach (Korisnik korisnik in Data.Instance.Korisnici)
				if (korisnik.obrisan == false)
					KorisnikList.Add(korisnik);
			PrikazKorisnika.Items.Refresh();

		}

		private void BtnDodaj_Click_1(object sender, RoutedEventArgs e)
		{
			

			Registracija a = new Registracija(null, "meni", null);
			a.Show();
			this.Close();
		}

		private void BtnIzmeni_Click_1(object sender, RoutedEventArgs e)
		{
			Korisnik korisnik = (Korisnik)PrikazKorisnika.SelectedItem;
			Registracija a = new Registracija(null, "izmena", korisnik);
			a.Show();
			this.Close();


		}

		private void btnPojedinacniPrikaz_Click(object sender, RoutedEventArgs e)
		{
			Korisnik korisnik = (Korisnik)PrikazKorisnika.SelectedItem;
			Prikazkorisnika pr = new Prikazkorisnika(korisnik);
			pr.Show();
		}

		private void BtnObrisi_Click_1(object sender, RoutedEventArgs e)
		{
			if (PrikazKorisnika.SelectedValue == null)
			{
				MessageBox.Show("Odaberi korisnika");
			}

			else if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
			{



				Korisnik korisnik = (Korisnik)PrikazKorisnika.SelectedItem;
				KorisnikList.Remove(korisnik);
				korisnik.obrisan = true;

				izmena.Instance.izmenaKorisnika(korisnik);
			
				PrikazKorisnika.Items.Refresh();

			}




		}

	








		private void Osvezi_Click(object sender, RoutedEventArgs e)
		{
			Reload();

			prezimeunos.Text= "";
			KorisnickoIme.Text = "";
			prezimeunos.Text= "";
			uloga.Text="";
		}





		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Data.Instance.UcitajSveKorisnike();
			KorisnikList.Clear();
			foreach (Korisnik korisnik in Data.Instance.Korisnici)
				if (korisnik.Ime == imeUnos.Text)
					KorisnikList.Add(korisnik);
			PrikazKorisnika.Items.Refresh();
		}

		

		private void Prezimeunos_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (prezimeunos.Text == "")
			{
				Reload();
			}
			else {

			Data.Instance.UcitajSveKorisnike();
			KorisnikList.Clear();
			foreach (Korisnik korisnik in Data.Instance.Korisnici)
				if (korisnik.Prezime.Contains(prezimeunos.Text))
					KorisnikList.Add(korisnik);
			PrikazKorisnika.Items.Refresh();
			 }

		}

		private void ImeUnos_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (imeUnos.Text == "")
			{
				Reload();
			}


			else
			{
				Data.Instance.UcitajSveKorisnike();
				KorisnikList.Clear();
				foreach (Korisnik korisnik in Data.Instance.Korisnici)
					if (korisnik.Ime.Contains(imeUnos.Text))
						KorisnikList.Add(korisnik);


				PrikazKorisnika.Items.Refresh();

			}
		}

		private void KorisnickoImePretraga_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (KorisnickoIme.Text == "")
			{
				Reload();
			}
			else
			{

				Data.Instance.UcitajSveKorisnike();
				KorisnikList.Clear();
				foreach (Korisnik korisnik in Data.Instance.Korisnici)
					if (korisnik.KorisnickoIme.Contains(KorisnickoIme.Text))
						KorisnikList.Add(korisnik);
				PrikazKorisnika.Items.Refresh();
			}
		}

		private void Uloga_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (KorisnickoIme.Text == "")
			{
				Reload();
			}
			else
			{

				Data.Instance.UcitajSveKorisnike();
				KorisnikList.Clear();
				foreach (Korisnik korisnik in Data.Instance.Korisnici)
					if (korisnik.TipKorisnika.Contains(uloga.Text))
						KorisnikList.Add(korisnik);
				PrikazKorisnika.Items.Refresh();
			}


		}



	}
}
